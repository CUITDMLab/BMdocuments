# R包

## Mixlm包

类型：标题混合模型方差分析和教育统计
主要功能：通过将函数r()添加到lm()和glm()的公式中,通过最小二乘法或REML来执行混合模型分析。还包括了高等教育的教科书统计数字,例如修改了lm()、glm()和相关的参数
如果检测到不支持的输入，则返回适当的方差分析或者暂停分析
Lm()函数在检测到不支持的输入时停止执行的包统计信息的包装。
Glm()函数用于适应广义线性模型,通过给出线性预测器的符号描述和误差分布的描述来指定。软件包提供的glm版本mixlm解析为混合建模的glmer。
- is.balanced 平衡模型的选择
- lm 拟合线性模型
- plotprops 物业小区的相关成分分析 
- PRESS 预测科幻TS
- print.AnovaMi 对于类的对象打印方法(AnovaMix)
- print.summary.lmm 总结线性模型拟合
- rparse 去除式函数r()。
- simple.glht有多个检测补偿成对比较。
- spearson 标准化皮尔森残差
- tally 离散的数字理货
- t_test 的t检验和z检验教科书版本。
主要功能是通过最小二乘，REML和将函数r()以流明的公式()和GLM()来执行混合模型分析。教科书的统计的高等教育的集合也包括在内，如MODI网络功能LM()，GLM()。
- Anova.lmm 方差分析与SS型II或III(包括混合模型)。
- anova.lmm 方差分析(连续SS)
- AnovaMix 方差的混合模型最小二乘分析(混合ANOVA)。
- anova_reg 方差分析的回归。
- best.subsets F-test 基于最好的分组选择
- CIgrandMean 对于线性模型的总平均值置信区间
- confusion 混淆矩阵。
- effect.labels 创建新的效果标签 LM
- forward F-test基于模型效果的选择线性模型。
- fparse 公式的影响。

## Rcompanion包
函数nagelkerke为各种模型类型提供伪R平方值,以及整个模型的似然比测试。为与厄米包匹配的模型提供了一个附加函数nagelkerkeHermite。
有几个函数提供分组数据的摘要统计信息。这些功能标题倾向于以"groupwise"开头。他们提供的手段,中线,几何手段,和Mestimators的团体,以及信心区间的传统方法和引导。
功能标题以"配对"开始进行分组测试,作为对综合测试的即席分析。在写作的时候,这些测试是情绪的中值测试,符号测试(为综合弗里德曼测试),置换测试,稳健方差分析,序号回归。输出是一个比较和p值的表,或者一个p值的矩阵,可以解析为一个紧凑的字母显示。
还有一些用于比较模型的函数.compareLM,compareGLM和pairwiseModelAnova。这些使用合适的措施,如AIC,BIC,BICc,或似然比测试.
有一些有用的绘图函数,包括plotNormalHistogram它绘制值的直方图并覆盖正常曲线,plotPredy为二元模型的预测值绘制直线。其他绘图功能包括生成密度图.

## Gpk包

这个包需要一个丰富的模型,并且可以成为初学者非常有用的构建块。
它应用广泛，如：
1、化妆品会引起用户过敏反应。这种发展可能对其生产者的名称造成重大损害。因此,进行安全测试是例行的。在一项研究中,4种产品的过敏性比较,七人被要求在前臂上应用每个产品。更高的刺激评分更糟糕的是产品。
2、牛奶增加嗜酸性粒细胞计数。血嗜酸性粒细胞异常增加导致气管狭窄。姜黄故事性有望减少牛奶的影响。

## BaylorEdPsych包
该包是将R数据帧羽为用于BILOG的固定宽度数据帧,并将其导出到.dat文件。类似于irtoys包中的est函数所做的,但允许使用您自己的ID变量而不是creading一个。
LittleMCAR(x)：根据样本大小和缺少数据模式的数量,此函数可能需要很长时间才能运行。

## Eeptools包
该包有用于清除字符串的函数,以及标识切口点，包括三个管理教育记录示例数据集,用于学习如何处理错误的记录。它还包括ggplot2图的一些主题,处理数据的任意文本文件,计算学生特征,以及在向量中查找阈值。未来的发展工作将包括调整和评估预警系统模型的方法。
其中age_calc函数以计算出生日期的年龄.他的功能计算从出生日期到另一个任意日期的天数、月份或年份的年龄。这将返回指定单位中的数值向量。
方便的函数收集，使有工作者或者行政者的记录更容易或更一致。功能包括清理字符串，并确定切点。此外，还包括行政教育记录三个示例数据集学习如何处理有错误的记录。
- age_calc 根据出生的日期计算年龄
- autoplot.lm为GGPLOT2线性模型复制的基本绘图功能。
- CLEANTEX 建立文档后，移除不想要的LaTeX files
- crosstabplot 绘制一个视觉交叉(MOSAID曲线)，用阴影用于在每个小区中的相关性和标签。
- crosstabs建立交叉表列表从数据集
- cutoff计算一个矢量累加和的阈值。
- decomma 从数值文件中删除逗号并返回数值
- defac 将系数转化为安全的字符串
- eeptools 教育政策工具的评价
- gelmansim 生成模型函数预测区间
- ggmapmerge 强化对SpatialPolygonsDataFrames绘制depcrecated方法
- lag_data 创建一个延迟
- leading_zero 函数中添加前导零维护固定成本宽度。
- makenum 一个函数来数值因素转换成数字类对象
- mapmerge 多边形转换成dataframes甲depcrecated方法联合的S4多边形对象与数据帧
- max_mis 可以安全地取最大值的载体，可能仅包括来港定居的
- midsch 总的测试成绩在中西部公立学校一个数据帧。
- moves_calc 函数来计算的时代学生已经改变了学校的数量。
- nth_max 查找第n个最大值
- profpoly 创建于GGPLOT2亲法fi ciency多边形用于显示评估类别
- profpoly.data 创建一个适合于在物体GGPLOT2构建自定义多边形层的数据帧
- remove_char在编辑后的数据与NA中的R取代的任意的字符像一个“*”
- retained_calc 计算学生是否已经重复了分数。
- statamode 可以模仿在Stata模式的功能。
- stuatt 根据战略数据项目属性工具包来分析学生的贡献
- stulevel K-12学生的合成数据集合属性。
- theme_dpi 在公开指示的Wisconsin部门对PDF和PNG的使用制定了弃用主题GGPLOT2
- theme_dpi_map 为PDF或SVG的地图开发的弃用主题GGPLOT2
- theme_dpi_map2 为PDF或SVG的地图开发了另一种过时的主题GGPLOT2
- theme_dpi_mapPNG 为PNG或JPG地图开发的弃用主题GGPLOT2
- thresh 由矢量的子集来表示返回累积和的最大百分比

## Educineq包
该包提供了计算基尼指数的可能性,也为灵敏度参数的不同值的广义熵测度。具体而言,该包包含计算平均对数偏差的函数,这对分布的底部更敏感;泰尔的熵测度,同样敏感的分布的所有部分;最后,将灵敏度参数设置为等于2的GE测量,使高等教育的差异更大。还提供了在国家间和国内不平等的组成部分中对这些措施的分解。还提供了两个图形工具,分析了教育素养分布的演变:累积分布函数和洛伦兹曲线。

## EdSurvey包
EdSurvey包用于分析精通数据的包.使用适当的方法对内存占用小的NCES数据集进行分析。包含在数据中的现有系统控制文件用于读取和格式化数据以进行进一步处理。
其中achievementLevels函数对每个edsurvey.data.frame应用适当的权重和方差估计方法,并为自定义分析结果的聚合和输出提供若干参数。也就是说,使用这些可选参数,用户可以选择生成每个成绩级别(离散)的学生百分比,在每个成绩级别(累计),计算学生的百分比分布成就水平(离散或累计)和选定的特征(在aggregateBy中指定),并计算在特定成绩水平内选定特征的学生百分比分布。
使用EdSurvey开始之前，请参阅覆盖安装，第一次使用的话，使用getData函数获取数据相比，可以进一步处理或分析，并利用住宿数据集。使用 browseVignettes看到小插图。这个包提供一个调用的函数 readNAEP 在NAEP数据里。 lm.sdf ，cor.sdf ，edsurveyTable ，和achievementLevels函数可用于分析数据，而 的getData 提取的感兴趣的作为用于进一步处理的data.frame的数据。

## simEd包
该包包含用于模拟教育的各种功能,包括简单的蒙特卡罗模拟函数、排队模拟函数、能够产生独立流的变量生成函数和对立变量,函数用于说明各种离散和连续分布的随机变量生成,以及计算时间持久统计的函数。还包含两个排队数据集(一个虚构的,一个真实世界),以方便输入模。

## BIFIEsurvey
包含调查统计工具(特别是在是在教育评估方面)为具有复制设计(jackknife, bootstrap, replicate weights(这三个应该是算法）)。描述性的统计，线性和对数回归，路径模型与测量误差修正和用于加权的采样两级分层回归清单变量都包括在内。统计的推断可以多重插补数据集和嵌套多重插补数据集进行。
- 均值和标准差( BIFIE.univar ) 频率( BIFIE.freq )
- 交叉表( BIFIE.crosstab )
- 线性回归( BIFIE.linreg )
- Logistic回归分析( BIFIE.logistreg )
- 与清单变量测量误差修正模型的路径( BIFIE.pathmodel )
- 分层数据的两个级别的回归( BIFIE.twolevelreg ; 随机斜率模型)
- 为导出的参数的统计推断( BIFIE.derivedParameters )
- 瓦尔德测试( BIFIE.waldtest ) 基于复制的统计模型的参数
- 用户自定义科幻 [R 功能 ( BIFIE.by )

## eefAnalytics
提供分析教育实验的工具。制作不同在一个地方访问的方法是用于教育试验的灵敏度的分析，在分析简单的随机试验，群随机试验和多位点试验使用不同的方法。
- caceCRTBoot 采用多层模型整群随机教育试验编者平均因果效应(CACE)分析。
- caceMSTBoot多站点随机教育试验CACE分析。
- caceSRTBoot 简单的随机教育试验CACE分析。
- ComparePlot标绘功能diferent eefAnalytics S3对象从eefAnalytics包进行比较。
- crtData 整群随机试验数据。
- crtFREQ 分析的使用多层次模型群随机教育试验。
- mlmBayes使用模糊先验随机educatuon试验Bayesia多层次的分析。
- mstData 多点试验数据。
- mstFREQ 分析使用多层模型多站点随机教育试验。
- plot.eefAnalytics 用于从eefAnalytic包获得的一个eefAnalytics S3对象的绘图方法。

## Heims
澳大利亚高等教育信息管理SYS-的解码元素 TEM(海姆斯)数据，为了清楚和性能。
- decoders 解码器
- decode_heims 解码海姆斯元素
- dummy_enrol 假招生网络文件
- element_decoders 让海姆斯元素号人类可读
- element_validation 验证海姆斯元素
- first_levels第一层次
- fread_heims 读取原始海姆斯网络文件
- heims_data_dict 海姆斯数据字典
- read_heims_fst 阅读从解码FST网络莱海姆斯数据
- relevel_heims Relevel分类变量
- utilities 实用功能

## rscorecard
一种方法来下载教育学院系使用公共API数据记分卡。它是基于管道命令的“dplyr”模型来选择和过滤数据在一个单一的链函数调用。从美国教育部门的API密钥是必需的。
- rscorecard:下载学院记分卡数据的方法。
- sc_dict :搜索数据字典。
- sc_filter: 过滤由变量值的记分卡的数据。
- sc_get: 获取记分卡数据。
- sc_init :初始化链接的请求。
- sc_key:在系统环境中存储Data.gov API密钥。
- sc_select :选择记分卡数据变量。
- sc_year :选择计分卡数据一年
- sc_zip 根据特殊代码地区的代码的人对结果分组

[BaylorEdPsych](https://cran.r-project.org/web/packages/BaylorEdPsych/index.html) | CRAN - Package BaylorEdPsych
[BIFIEsurvey](https://cran.r-project.org/web/packages/BIFIEsurvey/index.html) | CRAN - Package BIFIEsurvey
[EdSurvey](https://cran.r-project.org/web/packages/EdSurvey/index.html) | CRAN - Package EdSurvey
[educineq](https://cran.r-project.org/web/packages/educineq/index.html) | CRAN - Package educineq
[eefAnalytics](https://cran.r-project.org/web/packages/eefAnalytics/index.html) | CRAN - Package eefAnalytics
[eeptools](https://cran.r-project.org/web/packages/eeptools/index.html) | CRAN - Package eeptools
[gpk](https://cran.r-project.org/web/packages/gpk/index.html) | CRAN - Package gpk
[heims](https://cran.r-project.org/web/packages/heims/index.html) | CRAN - Package heims
[mixlm](https://cran.r-project.org/web/packages/mixlm/index.html) | CRAN - Package mixlm
[rcompanion](https://cran.r-project.org/web/packages/rcompanion/index.html) | CRAN - Package rcompanion
[rscorecard](https://cran.r-project.org/web/packages/rscorecard/index.html) | CRAN - Package rscorecard
[simEd](https://cran.r-project.org/web/packages/simEd/index.html) | CRAN - Package simEd